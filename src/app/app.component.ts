import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { tap, catchError } from 'rxjs/operators';

import { LogService } from './services/LogService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  private requestUrl : string;
  private includeBody: boolean;
  private requestBody: object;

  title = 'app';

  constructor (private httpClient : HttpClient, private logger : LogService ) {
    this.resetInputs();
  }

  onSubmit() : void {
    if (this.includeBody) {
      this.httpClient.post(this.requestUrl, this.requestBody).pipe(
        tap(response=>console.log("Respnose received for post request")),
        catchError(error => this.handleError(error))
      )
      .subscribe ( r => this.resetInputs());
    }
    else {
      this.httpClient.get(this.requestUrl, {responseType: 'text'}).pipe(
        tap (response => console.log("Response received")),
        catchError(error => this.handleError(error))
      )
      .subscribe ();
    }

  }

  handleError (error) : Observable<string> {
    return new ErrorObservable(error);
  }

  private resetInputs () : void {
    this.requestUrl = "";
    this.includeBody = false;
    this.requestBody = {}
  }
}
