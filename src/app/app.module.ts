import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { RequestLoggerInterceptor } from './services/RequestLoggerInterceptor';
import { LogService } from './services/LogService';
import { RequestBodyEditorComponent } from './components/request-body-editor/request-body-editor.component';
import { AddContentTypeHeaderInterceptor } from './services/AddContentTypeHeaderInterceptor';

@NgModule({
  declarations: [
    AppComponent,
    RequestBodyEditorComponent
  ],
  imports: [
    BrowserModule, FormsModule, HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestLoggerInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddContentTypeHeaderInterceptor,
      multi: true
    },
    LogService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
