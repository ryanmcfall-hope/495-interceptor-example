import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-request-body-editor',
  templateUrl: './request-body-editor.component.html',
  styleUrls: ['./request-body-editor.component.css']
})
export class RequestBodyEditorComponent implements OnInit {

  @Input() objectToEdit : object;

  inputType : string;
  fieldName: string;
  fieldValue: string;

  month: number;
  day: number;
  year: number;

  private months: String[] = [
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"
  ];

  private days : number[];

  constructor() {
    this.days = new Array<number> (31).fill(0).map( (value, index) => index+1);
    this.month = 0;
    this.day = 1;
  }

  ngOnInit() {
    this.inputType="string";
  }

  addField () : void {
    switch (this.inputType) {
      case "string":
        this.objectToEdit[this.fieldName] = this.fieldValue;
      break;

      case "number":
        this.objectToEdit[this.fieldName] = +this.fieldValue;
      break;

      case "date":
        this.objectToEdit[this.fieldName] = new Date(`${this.months[this.month]}/${this.day}/${this.year}`);
      break;
    }

    this.fieldName = "";
    this.fieldValue = "";
  }

}
