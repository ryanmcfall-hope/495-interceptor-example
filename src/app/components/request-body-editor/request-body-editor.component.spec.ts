import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestBodyEditorComponent } from './request-body-editor.component';

describe('RequestBodyEditorComponent', () => {
  let component: RequestBodyEditorComponent;
  let fixture: ComponentFixture<RequestBodyEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestBodyEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestBodyEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
