import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/observable';

@Injectable()
export class AddContentTypeHeaderInterceptor implements HttpInterceptor {

  intercept (request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {
    debugger
    let newRequest : HttpRequest<any> = request;

    if (request.method === "POST") {
        if (!request.headers.get("content-type")) {
          newRequest = request.clone({
            headers: request.headers.set('x-content-type', 'application-json')
          });
        }
    }
    return next.handle(newRequest);
  }
}
