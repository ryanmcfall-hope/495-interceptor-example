import { Injectable } from '@angular/core'
import { HttpInterceptor, HttpEvent, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { LogService, LogRecord } from './LogService';

import { Observable } from 'rxjs/observable';
import { tap, filter, catchError } from 'rxjs/operators';

@Injectable()
export class RequestLoggerInterceptor implements HttpInterceptor {

  constructor (private logger : LogService ) {}

  public intercept (request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {
    let logRecord : LogRecord = this.logger.addRequest(new Date(), request.url);
    return next.handle(request).pipe(
      filter( e => e instanceof HttpResponse),
      tap(e => this.handleNext(e, logRecord)),
      catchError(error => this.handleError(error, logRecord))
    );
  }

  handleNext (e : HttpEvent<any>, requestRecord : LogRecord) : void {
    debugger
    let response : HttpResponse<any> = <HttpResponse<any>> e;
    this.fillInLogRecordFromResponse(requestRecord, response);
  }

  handleError (error : any, requestRecord : LogRecord) : Observable<HttpEvent<any>> {
    debugger
    let response : HttpErrorResponse = <HttpErrorResponse> error;
    this.fillInLogRecordFromResponse(requestRecord, response);
    return error;
  }

  private fillInLogRecordFromResponse (logRecord : LogRecord, response : HttpResponse<any> | HttpErrorResponse) {
    logRecord.responseDate = new Date();
    logRecord.responseCode = response.status;
    logRecord.contentType = response.headers.get('Content-type');
    logRecord.contentLength = +response.headers.get('Content-length');
  }
}
