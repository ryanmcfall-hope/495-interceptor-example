import { Injectable } from '@angular/core';

const MILLIS_PER_SECOND = 1000;
const MILLIS_PER_MINUTE = MILLIS_PER_SECOND * 60;

export class LogRecord {

  constructor (
    public type?: string,
    public requestDate?: Date,
    public url?: string,    
    public responseDate?: Date,
    public responseCode? : number,
    public contentType? : string,
    public contentLength? : number,
    public body? : string
  )
  {}

  get timeForRequest() : string {
    if (this.responseDate === undefined) {
      return "Unknown";
    }

    let responseTimeInMillis : number = this.responseDate.getTime();
    let requestTimeInMillis : number = this.requestDate.getTime();
    let timeInMillis = responseTimeInMillis - requestTimeInMillis;

    //  Time was at least minute
    if (timeInMillis >= MILLIS_PER_MINUTE) {
      let minutes : number = Math.floor(timeInMillis / MILLIS_PER_MINUTE);
      let seconds : number = timeInMillis - (minutes * MILLIS_PER_MINUTE);
      return `${minutes} minutes, ${seconds} seconds`;
    }

    //  Time was at least one second
    if (timeInMillis >= MILLIS_PER_SECOND) {
      let seconds :number = Math.floor(timeInMillis / MILLIS_PER_SECOND);
      let milliseconds :number = timeInMillis - (seconds * MILLIS_PER_SECOND);
      return `${seconds}.${milliseconds} seconds`;
    }

    //  Time below one second
    return `${timeInMillis} ms`;
  }
}

@Injectable()
export class LogService {
  private _logRecords : Array<LogRecord>;
  constructor () {
    this._logRecords = new Array<LogRecord> ();
  }

  public get logRecords () {
    return this._logRecords;
  }

  public addRequest (date : Date, url : string) : LogRecord {
    let logRecord = new LogRecord ('Request', date, url);
    this._logRecords.unshift(logRecord);
    return logRecord;
  }
}
